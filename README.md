# Herschel-Bulkleyモデルでの流体中の応力

2023/11/29  
M.TANAKA  


[TOC]

## 1. 概要

非ニュートン流体モデルを使ったことがないので，テストしてみる．
ついでに応力テンソル出せそうなら出してみる．

## 2. 対象とするケース

単純なもので良いので，[軸対称ポアズイユ流れ](https://develop.openfoam.com/Development/openfoam/-/tree/develop/tutorials/incompressible/simpleFoam/pipeCyclic)とする．

## 3. 変更点

- 入口の旋回流を軸に平行な流れに変更(`0.org/U`)
- 入口を圧力によって駆動される流れに変更(`0.org/p`)
- functionの`coordinateTransform`，`momentum`を削除(`system/controlDict`)
- メッシュの細分化(system/blockMeshDict)
- 乱流モデルをなくす(`constant/turbulenceProperties`)
- ニュートン流体→粘塑性流体(`constant/transportProperties`) ←次節に詳しい

### 3.1 Herschel-Bulkleyモデルの設定

全く詳しくないため，下記論文の設定をそのまま利用した．論文は平板間であり，個の解析は円管であるので，同じ値で良いのか疑問は残る．しかし今回はとにかくそれらしい結果がでれば良しとする．
- [川崎 浩司他，非ニュートン流体解析に対するOpenFOAMとDualSPHysicsの比較検討](https://www.jstage.jst.go.jp/article/jscejoe/77/2/77_I_49/_article/-char/ja)

## 4. 結果

結構，元の論文のものと似た結果が得られた．

![no image](./img/U_fromPaper.png)
↑論文より，降伏応力を変えたときの速度分布．

![no image](./img/U_and_nu.png)
今回計算した結果．右は流速Uと動粘性係数nuの分布．降伏応力は20Paに合わせた．

変形しない領域は，nuが莫大なので判別できる．


