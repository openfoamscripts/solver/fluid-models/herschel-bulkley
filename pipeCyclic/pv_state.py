# state file generated using paraview version 5.10.1

# uncomment the following three lines to ensure this script works in future versions
#import paraview
#paraview.compatibility.major = 5
#paraview.compatibility.minor = 10

#### import the simple module from the paraview
import os
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# Create a new 'Line Chart View'
lineChartView1 = CreateView('XYChartView')
lineChartView1.ViewSize = [796, 913]
lineChartView1.LegendPosition = [664, 854]
lineChartView1.LeftAxisUseCustomRange = 1
lineChartView1.LeftAxisRangeMinimum = -0.2212728603252383
lineChartView1.LeftAxisRangeMaximum = 4.154437459931609
lineChartView1.BottomAxisUseCustomRange = 1
lineChartView1.BottomAxisRangeMinimum = -0.024977055803617587
lineChartView1.BottomAxisRangeMaximum = 0.5281931766409208
lineChartView1.RightAxisUseCustomRange = 1
lineChartView1.RightAxisRangeMinimum = -14.83237369171359
lineChartView1.RightAxisRangeMaximum = 17.547882678187097
lineChartView1.TopAxisUseCustomRange = 1
lineChartView1.TopAxisRangeMinimum = 1.6360648713645616
lineChartView1.TopAxisRangeMaximum = 9.004292367526029

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [796, 913]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [5.0, 0.0, 0.25]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [15.58582853266182, 1.3710611436276399, -5.508672612661314]
renderView1.CameraFocalPoint = [6.549011729802692, -0.10109538645523371, 0.4892600107300276]
renderView1.CameraViewUp = [-0.42626689859007877, -0.4876201189151314, -0.761920698495315]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 5.018714947251644
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

# init the 'GridAxes3DActor' selected for 'AxesGrid'
renderView1.AxesGrid.Visibility = 1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.SplitHorizontal(0, 0.500000)
layout1.AssignView(1, renderView1)
layout1.AssignView(2, lineChartView1)
layout1.SetSize(1593, 913)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
dir = os.path.dirname(__file__)
pipeCyclicfoam = OpenFOAMReader(registrationName='pipeCyclic.foam', FileName=f'{dir}/pipeCyclic.foam')
pipeCyclicfoam.MeshRegions = ['internalMesh']
pipeCyclicfoam.CellArrays = ['U', 'p', 'epsilon', 'k', 'nut', 'nu']

# create a new 'Plot Over Line'
plotOverLine1 = PlotOverLine(registrationName='PlotOverLine1', Input=pipeCyclicfoam)
plotOverLine1.Point1 = [5.0, 0.0, 0.0]
plotOverLine1.Point2 = [5.0, 0.0, 0.5]

# ----------------------------------------------------------------
# setup the visualization in view 'lineChartView1'
# ----------------------------------------------------------------

# show data from plotOverLine1
plotOverLine1Display = Show(plotOverLine1, lineChartView1, 'XYChartRepresentation')

# trace defaults for the display properties.
plotOverLine1Display.UseIndexForXAxis = 0
plotOverLine1Display.XArrayName = 'arc_length'
plotOverLine1Display.SeriesVisibility = ['nu', 'U_Magnitude']
plotOverLine1Display.SeriesLabel = ['arc_length', 'arc_length', 'epsilon', 'epsilon', 'k', 'k', 'nut', 'nut', 'p', 'p', 'U_X', 'U_X', 'U_Y', 'U_Y', 'U_Z', 'U_Z', 'U_Magnitude', 'U_Magnitude', 'vtkValidPointMask', 'vtkValidPointMask', 'Points_X', 'Points_X', 'Points_Y', 'Points_Y', 'Points_Z', 'Points_Z', 'Points_Magnitude', 'Points_Magnitude', 'nu', 'nu']
plotOverLine1Display.SeriesColor = ['arc_length', '0', '0', '0', 'epsilon', '0.8899977111467154', '0.10000762951094835', '0.1100022888532845', 'k', '0.220004577706569', '0.4899977111467155', '0.7199969481956207', 'nut', '0.30000762951094834', '0.6899977111467155', '0.2899977111467155', 'p', '0.6', '0.3100022888532845', '0.6399938963912413', 'U_X', '1', '0.5000076295109483', '0', 'U_Y', '0.6500038147554742', '0.3400015259021897', '0.16000610360875867', 'U_Z', '0', '0', '0', 'U_Magnitude', '0.8899977111467154', '0.10000762951094835', '0.1100022888532845', 'vtkValidPointMask', '0.220004577706569', '0.4899977111467155', '0.7199969481956207', 'Points_X', '0.30000762951094834', '0.6899977111467155', '0.2899977111467155', 'Points_Y', '0.6', '0.3100022888532845', '0.6399938963912413', 'Points_Z', '1', '0.5000076295109483', '0', 'Points_Magnitude', '0.6500038147554742', '0.3400015259021897', '0.16000610360875867', 'nu', '0.6', '0.3100022888532845', '0.6399938963912413']
plotOverLine1Display.SeriesPlotCorner = ['Points_Magnitude', '0', 'Points_X', '0', 'Points_Y', '0', 'Points_Z', '0', 'U_Magnitude', '0', 'U_X', '0', 'U_Y', '0', 'U_Z', '0', 'arc_length', '0', 'epsilon', '0', 'k', '0', 'nu', '0', 'nut', '0', 'p', '0', 'vtkValidPointMask', '0']
plotOverLine1Display.SeriesLabelPrefix = ''
plotOverLine1Display.SeriesLineStyle = ['Points_Magnitude', '1', 'Points_X', '1', 'Points_Y', '1', 'Points_Z', '1', 'U_Magnitude', '1', 'U_X', '1', 'U_Y', '1', 'U_Z', '1', 'arc_length', '1', 'epsilon', '1', 'k', '1', 'nu', '1', 'nut', '1', 'p', '1', 'vtkValidPointMask', '1']
plotOverLine1Display.SeriesLineThickness = ['Points_Magnitude', '2', 'Points_X', '2', 'Points_Y', '2', 'Points_Z', '2', 'U_Magnitude', '2', 'U_X', '2', 'U_Y', '2', 'U_Z', '2', 'arc_length', '2', 'epsilon', '2', 'k', '2', 'nu', '2', 'nut', '2', 'p', '2', 'vtkValidPointMask', '2']
plotOverLine1Display.SeriesMarkerStyle = ['Points_Magnitude', '0', 'Points_X', '0', 'Points_Y', '0', 'Points_Z', '0', 'U_Magnitude', '0', 'U_X', '0', 'U_Y', '0', 'U_Z', '0', 'arc_length', '0', 'epsilon', '0', 'k', '0', 'nu', '0', 'nut', '0', 'p', '0', 'vtkValidPointMask', '0']
plotOverLine1Display.SeriesMarkerSize = ['Points_Magnitude', '4', 'Points_X', '4', 'Points_Y', '4', 'Points_Z', '4', 'U_Magnitude', '4', 'U_X', '4', 'U_Y', '4', 'U_Z', '4', 'arc_length', '4', 'epsilon', '4', 'k', '4', 'nu', '4', 'nut', '4', 'p', '4', 'vtkValidPointMask', '4']

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from pipeCyclicfoam
pipeCyclicfoamDisplay = Show(pipeCyclicfoam, renderView1, 'UnstructuredGridRepresentation')

# get color transfer function/color map for 'nu'
nuLUT = GetColorTransferFunction('nu')
nuLUT.RGBPoints = [0.01168069988489151, 0.231373, 0.298039, 0.752941, 2.0058403499424458, 0.865003, 0.865003, 0.865003, 4.0, 0.705882, 0.0156863, 0.14902]
nuLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'nu'
nuPWF = GetOpacityTransferFunction('nu')
nuPWF.Points = [0.01168069988489151, 0.0, 0.5, 0.0, 4.0, 1.0, 0.5, 0.0]
nuPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
pipeCyclicfoamDisplay.Representation = 'Surface'
pipeCyclicfoamDisplay.ColorArrayName = ['CELLS', 'nu']
pipeCyclicfoamDisplay.LookupTable = nuLUT
pipeCyclicfoamDisplay.SelectTCoordArray = 'None'
pipeCyclicfoamDisplay.SelectNormalArray = 'None'
pipeCyclicfoamDisplay.SelectTangentArray = 'None'
pipeCyclicfoamDisplay.OSPRayScaleArray = 'p'
pipeCyclicfoamDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
pipeCyclicfoamDisplay.SelectOrientationVectors = 'U'
pipeCyclicfoamDisplay.SelectScaleArray = 'p'
pipeCyclicfoamDisplay.GlyphType = 'Arrow'
pipeCyclicfoamDisplay.GlyphTableIndexArray = 'p'
pipeCyclicfoamDisplay.GaussianRadius = 0.05
pipeCyclicfoamDisplay.SetScaleArray = ['POINTS', 'p']
pipeCyclicfoamDisplay.ScaleTransferFunction = 'PiecewiseFunction'
pipeCyclicfoamDisplay.OpacityArray = ['POINTS', 'p']
pipeCyclicfoamDisplay.OpacityTransferFunction = 'PiecewiseFunction'
pipeCyclicfoamDisplay.DataAxesGrid = 'GridAxesRepresentation'
pipeCyclicfoamDisplay.PolarAxes = 'PolarAxesRepresentation'
pipeCyclicfoamDisplay.ScalarOpacityFunction = nuPWF
pipeCyclicfoamDisplay.ScalarOpacityUnitDistance = 0.469844686517974
pipeCyclicfoamDisplay.OpacityArrayName = ['POINTS', 'p']

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
pipeCyclicfoamDisplay.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 0.07635270059108734, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
pipeCyclicfoamDisplay.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 0.07635270059108734, 1.0, 0.5, 0.0]

# setup the color legend parameters for each legend in this view

# get color legend/bar for nuLUT in view renderView1
nuLUTColorBar = GetScalarBar(nuLUT, renderView1)
nuLUTColorBar.WindowLocation = 'Upper Right Corner'
nuLUTColorBar.Title = 'nu'
nuLUTColorBar.ComponentTitle = ''

# set color bar visibility
nuLUTColorBar.Visibility = 1

# show color legend
pipeCyclicfoamDisplay.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# restore active source
SetActiveSource(pipeCyclicfoam)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')